#include "canbus.hpp"
#include "std_msgs/Header.h"
#include <time.h>
namespace ns_can{
    //constructor
    CanBus::CanBus(ros::NodeHandle &nodeHandle):nodeHandle_(nodeHandle){
        loadParameters();
        subscribeToTopics();
        publishToTopics();
    }

    void CanBus::loadParameters(void){
        ROS_INFO("loading handle parameters");
        //pub topic name
        if (!nodeHandle_.param<std::string>("asensing_data_topic_name", asensing_data_topic_name_, "asensing_data")) {
            ROS_WARN_STREAM("Did not load asensing_data_topic_name. Standard value is: " << asensing_data_topic_name_);
        }
        if (!nodeHandle_.param<std::string>("res_and_ami_data_topic_name", res_and_ami_data_topic_name_, "res_and_ami_data")) {
            ROS_WARN_STREAM("Did not load res_and_ami_data_topic_name. Standard value is: " << res_and_ami_data_topic_name_);
        }
        if (!nodeHandle_.param<std::string>("steering_motor_data_topic_name", steering_motor_data_topic_name_, "steering_motor_receive_data")) {
            ROS_WARN_STREAM("Did not load steering_motor_data_topic_name. Standard value is: " << steering_motor_data_topic_name_);
        }
        if (!nodeHandle_.param<std::string>("steering_sensor_data_topic_name", steering_sensor_data_topic_name_, "steering_sensor_data")) {
            ROS_WARN_STREAM("Did not load steering_sensor_data_topic_name. Standard value is: " << steering_sensor_data_topic_name_);
        }
        if (!nodeHandle_.param<std::string>("feedback_data_topic_name", feedback_data_topic_name_, "wheel_and_motor_data")) {
            ROS_WARN_STREAM("Did not load feedback_data_topic_name. Standard value is: " << feedback_data_topic_name_);
        }
        if (!nodeHandle_.param<std::string>("ecu_steering_command_data_topic_name", ecu_steering_command_data_topic_name_, "steering_ecu_command_receive_data")) {
            ROS_WARN_STREAM("Did not load ecu_steering_command_data_topic_name. Standard value is: " << ecu_steering_command_data_topic_name_);
        }
        if (!nodeHandle_.param<std::string>("control_command_to_socketcan_topic_name", control_command_to_socketcan_topic_name_, "/sent_messages")) {
            ROS_WARN_STREAM("Did not load control_command_to_sockecan_topic_name. Standard value is: " << control_command_to_socketcan_topic_name_);
        }
        //sub topic name
        if (!nodeHandle_.param<std::string>("ros_canopen_topic_name", ros_canopen_topic_name_, "/received_messages")) {
            ROS_WARN_STREAM("Did not load ros_canopen_topic_name. Standard value is: " << ros_canopen_topic_name_);
        }
        if (!nodeHandle_.param<std::string>("control_command_topic_name", control_command_topic_name_, "/control/pure_pursuit/control_command")) {
            ROS_WARN_STREAM("Did not load control_command_topic_name. Standard value is: " << control_command_topic_name_);
        }
        if (!nodeHandle_.param<std::string>("asstate_data_topic_name", asstate_data_topic_name_, "/asState")) {
            ROS_WARN_STREAM("Did not load asstate_data_topic_name. Standard value is: " << asstate_data_topic_name_);
        }
    }

    void CanBus::publishToTopics(void){
        ROS_INFO("publish to topics");
        asensingDataPublisher_  = nodeHandle_.advertise<fsd_common_msgs::AsensingMessage>(asensing_data_topic_name_,10);
        resAndAmiPublisher_ = nodeHandle_.advertise<fsd_common_msgs::ResAndAmi>(res_and_ami_data_topic_name_,10);
        steeringMotorDataPublisher_ = nodeHandle_.advertise<fsd_common_msgs::ControlCommand>(steering_motor_data_topic_name_,10);
        steeringSensorDataPublisher_ = nodeHandle_.advertise<fsd_common_msgs::ControlCommand>(steering_sensor_data_topic_name_,10);
        feedbackDataPublisher_ = nodeHandle_.advertise<fsd_common_msgs::Feedback>(feedback_data_topic_name_,10);
        ecuSteeringCommandDataPublisher_ = nodeHandle_.advertise<fsd_common_msgs::ControlCommand>(ecu_steering_command_data_topic_name_,10);
        controlCommandPublisher_ = nodeHandle_.advertise<can_msgs::Frame>(control_command_to_socketcan_topic_name_,10);
    }
    void CanBus::subscribeToTopics(void){
        ROS_INFO("subscribe to topcis");
        rosCanOpenSubscriber_ = nodeHandle_.subscribe<can_msgs::Frame>(ros_canopen_topic_name_,50,&CanBus::rosCanOpenCallback,this);
        // rosCanOpenSubscriber_ = nodeHandle_.subscribe<fsd_common_msgs::CanFrames>(ros_canopen_topic_name_,1000,&CanBus::rosCanOpenCallback,this);
        controlCommandSubscriber_ = nodeHandle_.subscribe<fsd_common_msgs::ControlCommand>(control_command_topic_name_,1,&CanBus::controlCommandCallback,this);
        asStateSubscriber_ = nodeHandle_.subscribe<fsd_common_msgs::AsState>(asstate_data_topic_name_,1,&CanBus::asStateCallback,this);
    }
    
// void CanBus::rosCanOpenCallback(const fsd_common_msgs::CanFrames::ConstPtr& frame){
void CanBus::rosCanOpenCallback(const can_msgs::Frame::ConstPtr& frame){
        //ROS_INFO("processing can");
        // auto canid = frame_from_can.id;
        // auto dlc = frame_from_can.dlc;

        //std::cout<<"current id is"<<canid<<std::endl;
        //begin to resolve
        
        
        
        can_msgs::Frame frame_from_can = *frame;
        clock_t start_time;
        clock_t end_time;
        start_time= ros::Time::now().toNSec();
        
        // fsd_common_msgs::CanFrames frame_from_can = *frame;
        // int num_frame = frame_from_can.Frames.size();
        // std::cout<<"============="<<std::endl;
        // std::cout<<"this is num_frame"<<"   "<<num_frame<<"个"<<std::endl;
        // for(int i = 0; i < num_frame; i++)
        // {
            
            auto canid = frame_from_can.id;
            //检测时候用
            //std::cout<<i<<std::endl;
            
            // auto dlc = frame_from_can.dlc;

            switch (canid)
            {
                
            //0. CAN ID: 0x35B (旧版电机程序是 0x01 0XA6)  
            case 859:{
                //前四位是电机返回值 #counts
                //后四位是ecu发送给转向电机的指令  #counts
                //均高位在前，低位在后
                auto motor_return_num1 = frame_from_can.data[0];
                auto motor_return_num2 = frame_from_can.data[1];
                auto motor_return_num3 = frame_from_can.data[2];
                auto motor_return_num4 = frame_from_can.data[3];
                auto ecu_send_num1 = frame_from_can.data[4];
                auto ecu_send_num2 = frame_from_can.data[5];
                auto ecu_send_num3 = frame_from_can.data[6];
                auto ecu_send_num4 = frame_from_can.data[7];

                //电机返回值
                auto temp1 = -(256 * 256 * 256 * motor_return_num1 + 256 * 256 * motor_return_num2 + 256 * motor_return_num3 + motor_return_num4);
                std::cout<<"电机返回值是："<<temp1<<"counts"<<std::endl;
                auto steering_angle = (-(256 * 256 * 256 * motor_return_num1 + 256 * 256 * motor_return_num2 + 256 * motor_return_num3 + motor_return_num4) - steering_motor_coe_b_ ) / steering_motor_coe_k_ * M_PI / 180;
                steering_motor_data_.header.stamp = ros::Time::now();
                steering_motor_data_.steering_angle.data = static_cast<float>(steering_angle);
                steeringMotorDataPublisher_.publish(steering_motor_data_);

                //监听并转发ecu发给转向电机的转角到ROS
                auto temp2 = -(256 * 256 * 256 * ecu_send_num1 + 256 * 256 * ecu_send_num2 + 256 * ecu_send_num3 + ecu_send_num4);
                std::cout<<"电机返回值是："<<temp2<<"counts"<<std::endl;
                auto ecu_steering_command = (-1*(256*256*256*ecu_send_num1 + 256*256*ecu_send_num2 + 256*ecu_send_num3 + ecu_send_num4) - steering_motor_coe_b_) / steering_motor_coe_k_ * M_PI / 180;
                ecu_steering_command_data_.header.stamp = ros::Time::now();
                ecu_steering_command_data_.steering_angle.data = static_cast<float>(ecu_steering_command);
                ecuSteeringCommandDataPublisher_.publish(ecu_steering_command_data_);
            
                break;
            }
            
            //1. can id: 0x306
            case 774:{
                auto motor_command_speed_rl = 0.00868 * (256 * frame_from_can.data[0] + frame_from_can.data[1]);
                auto motor_command_speed_rr = 0.00868 * (256 * frame_from_can.data[6] + frame_from_can.data[7]);
                feedback_data_.motor_command_speed_rl = static_cast<float>(motor_command_speed_rl);
                feedback_data_.motor_command_speed_rr = static_cast<float>(motor_command_speed_rr);
                break;
            }

            //2. CAN ID: 0X358
            //导远570d输入的轮速对应的CAN ID为0X348(即840)
            case 856:{
                auto wheel_speed_fl = 0.00868 * (256 * frame_from_can.data[0] + frame_from_can.data[1]);
                auto wheel_speed_fr = 0.00868 * (256 * frame_from_can.data[2] + frame_from_can.data[3]);
                feedback_data_.wheel_speed_fl = static_cast<float>(wheel_speed_fl);
                feedback_data_.wheel_speed_fr = static_cast<float>(wheel_speed_fr);
                break;
            }

            //3. CAN ID: 0X219
            case 537:{
                auto motor_return_speed_rl = ((frame_from_can.data[0] + 256 * frame_from_can.data[1]) / 2 - 10000) / 60.0 / 3.517 * M_PI * 2 * 0.197;
                auto motor_return_torq_rl = (frame_from_can.data[2] + 256 * frame_from_can.data[3]);
                feedback_data_.motor_return_speed_rl = static_cast<float>(motor_return_speed_rl);
                feedback_data_.motor_return_torq_rl = static_cast<float>(motor_return_torq_rl);
                break;
            }

            //4. CAN ID: 0X217
            case 535:{
                auto motor_return_speed_rr = ((frame_from_can.data[0] + 256 * frame_from_can.data[1]) / 2 - 10000) / 60.0 / 3.517 * M_PI * 2 * 0.197;
                auto motor_return_torq_rr = (frame_from_can.data[2] + 256 * frame_from_can.data[3]);
                feedback_data_.motor_return_speed_rr = static_cast<float>(motor_return_speed_rr);
                feedback_data_.motor_return_torq_rr = static_cast<float>(motor_return_torq_rr);
                break;
            }

            //5. CAN ID: 0X205 AMI任务选择 & 方向盘转角传感器
            //实验先改成0x509
            case 517:{
                auto resState_index = frame_from_can.data[2];
                auto mission_index = frame_from_can.data[3];
                // std::cout<<mission_index<<std::endl;
                //让模式选择一点时间
                int start_index = mission_index;
                if(end_index != start_index){
                    end_index = start_index;
                    mission_start_time = ros::Time::now().toSec();
                }
                std::cout << "our mission will choose after"<< 4 - (ros::Time::now().toSec() - mission_start_time) << "sec" << std::endl;
                if(end_index == start_index && (ros::Time::now().toSec() - mission_start_time) > 4.0){
                    choose_mission_index = end_index;
                    std::cout<<"we choose the mission"<<std::endl;
                }
                auto steering_sensor_data =  M_PI / 180 * (frame_from_can.data[5] - 90) / 5;
                steering_sensor_data_.header.stamp = ros::Time::now();
                steering_sensor_data_.steering_angle.data = static_cast<float>(steering_sensor_data);
                steeringSensorDataPublisher_.publish(steering_sensor_data_);
                std::cout << "we got the frame" << std::endl;
                res_and_ami_data_.header.stamp = ros::Time::now();
                ((resState_index == 3) ? (res_and_ami_data_.resState = 1) : (res_and_ami_data_.resState = 0));//1 表示go信号
                //指示AMI选择的无人驾驶任务。0.manual driving;1.acceleration;2.skidpad;3.trackdrive;4.ebs_test;5.inspection
                switch (choose_mission_index){
                    case 1:
                        res_and_ami_data_.mission = "acceleration";
                        break;
                    case 2:
                        res_and_ami_data_.mission = "skidpad";
                        break;
                    case 3:
                        res_and_ami_data_.mission = "trackdrive";
                        break;
                    case 4:
                        res_and_ami_data_.mission = "ebs_test";
                        break;
                    case 5:
                        res_and_ami_data_.mission = "inspection";
                        break;
                    default:
                        res_and_ami_data_.mission = "";
                        std::cout << "we got no mission, and the mission should be manual driving hhhh" << std::endl;
                        break;
                }
                resAndAmiPublisher_.publish(res_and_ami_data_);
                break;
            }
            //6-14为惯导输出数据，具体的协议解析可以查阅ins570d的使用手册
            //6. CAN ID: 0X500
            case 1280:{
                auto Acc_x = 0.0001220703125 * (256 * frame_from_can.data[0] + frame_from_can.data[1]) - 4;
                auto Acc_y = 0.0001220703125 * (256 * frame_from_can.data[2] + frame_from_can.data[3]) - 4;
                auto Acc_z = 0.0001220703125 * (256 * frame_from_can.data[4] + frame_from_can.data[5]) - 4;
                asensing_data_.header.stamp = ros::Time::now();
                asensing_data_.Acc_x = static_cast<float>(Acc_x);
                asensing_data_.Acc_y = static_cast<float>(- Acc_y);
                asensing_data_.Acc_z = static_cast<float>(- Acc_z);

                if (asensing_data_.Status == 2){
               	std::cout <<"######################成功发布组合惯导topic！######################" << std::endl;
                }
                else{
                    std::cout<<" do not initialize the GNSS"<<std::endl;
                    //std::cout <<"######################组合惯导未初始化成功！请检查天线和组合惯导指示灯#！####################" << std::endl;
                }
                //std::cout<<"fucking send a ros message"<<std::endl;
                asensingDataPublisher_.publish(asensing_data_);
                break;
            }

            //7. CAN ID: 0X501
            case 1281:{
                auto Gyro_x = 0.0076293 * (256 * frame_from_can.data[0] + frame_from_can.data[1]) - 250;
                auto Gyro_y = 0.0076293 * (256 * frame_from_can.data[2] + frame_from_can.data[3]) - 250;
                auto Gyro_z = 0.0076293 * (256 * frame_from_can.data[4] + frame_from_can.data[5]) - 250;
                asensing_data_.Gyro_x = static_cast<float>(Gyro_x);
                asensing_data_.Gyro_y = static_cast<float>(Gyro_y);
                asensing_data_.Gyro_z = static_cast<float>(Gyro_z);
                std::cout<<Gyro_x<<std::endl;
                break;
            }

            //8. CAN ID: 0x502
            case 1282:{
                auto pitch = 0.010986 * (256 * frame_from_can.data[0] + frame_from_can.data[1]) - 360;
                auto roll = 0.010986 * (256 * frame_from_can.data[2] + frame_from_can.data[3]) - 360;
                auto heading_angle = 0.010986 * (256 * frame_from_can.data[4] + frame_from_can.data[5]) - 360;

                asensing_data_.pitch = static_cast<float>(pitch);
                asensing_data_.roll = static_cast<float>(roll);
                asensing_data_.heading_angle = static_cast<float>(heading_angle);
                break;
            }

            //9. CAN ID: 0X503
            case 1283:{
                auto h = 0.001 * (256 * 256 * 256 * frame_from_can.data[0] + 256 * 256 * frame_from_can.data[1] + 256 * frame_from_can.data[2] + frame_from_can.data[3]) - 10000;
                auto ullTimestamp = 256 * 256 * 256 * frame_from_can.data[4] + 256 * 256 * frame_from_can.data[5] + 256 * frame_from_can.data[6] + frame_from_can.data[7];

                asensing_data_.h = static_cast<float>(h);
                asensing_data_.ullTimestamp = static_cast<uint64_t>(ullTimestamp);
                break;
            }

            //10. CAN ID: 0X504
            //经纬度转为xy坐标
            case 1284:{
                auto lat = 1e-7 * (256 * 256 * 256 * frame_from_can.data[0] + 256 * 256 * frame_from_can.data[1] + 256 * frame_from_can.data[2] + frame_from_can.data[3]) - 180;
                auto lon = 1e-7 * (256 * 256 * 256 * frame_from_can.data[4] + 256 * 256 * frame_from_can.data[5] + 256 * frame_from_can.data[6] + frame_from_can.data[7]) - 180;

                //参考19/20赛季的tf_coordinate_broad.cpp中的转换方法trans_lonlat_to_xy，通过投影关系将经纬度信息转化为m为单位的距离信息
                lon = lon * M_PI /180;
                lat = lat * M_PI /180;
                double lat0 =30* 3 /180;
                double N = 0, e = 0, a = 0, b = 0, e2 = 0, K = 0;
                a =6378137;
                b =6356752.3142;
                e = sqrt(1- (b / a) * (b / a));
                e2 = sqrt((a / b) * (a / b) -1);
                double coslat0 = cos(lat0);
                N = (a * a / b) / sqrt(1+ e2 * e2 * coslat0 * coslat0);
                K = N * coslat0;
                double Pi = M_PI;
                double sinlat = sin(lat);
                double tan1 = tan(Pi /4+ lat /2);
                double E2 = pow((1- e * sinlat) / (1+ e * sinlat), e /2);
                double xx = tan1 * E2;
                auto lat_m = K * log(xx) - init_lat_m_; //由于直接转换后的经纬度数据绝对值很大，应此在此将其减去一个数值，使其大小变为较小数值
                auto lon_m = K * lon - init_lon_m_;

                asensing_data_.lat = static_cast<double>(lat_m);
                asensing_data_.lon = static_cast<double>(lon_m);
                break;
            }

            //11. CAN ID: 505
            case 1285:{
                auto Vn = 0.0030517 * (256 * frame_from_can.data[0] + frame_from_can.data[1]) - 100;
                auto Ve = 0.0030517 * (256 * frame_from_can.data[2] + frame_from_can.data[3]) - 100;
                auto Vh = 0.0030517 * (256 * frame_from_can.data[4] + frame_from_can.data[5]) - 100;

                asensing_data_.Vn = static_cast<float>(Vn);
                asensing_data_.Ve = static_cast<float>(Ve);
                asensing_data_.Vh = static_cast<float>(Vh);
                break;
            }

            //12. CAN ID: 0X506
            case 1286:{
                auto GpsFlag_Pos = frame_from_can.data[0];
                auto NumSV = frame_from_can.data[1];
                auto GpsFlag_Heading = frame_from_can.data[2];
                auto GPS_Age = frame_from_can.data[3];
                auto Car_Status = frame_from_can.data[4];
                auto Status = frame_from_can.data[7];

                asensing_data_.GpsFlag_Pos = static_cast<uint32_t>(GpsFlag_Pos);
                asensing_data_.NumSV = static_cast<uint32_t>(NumSV);
                asensing_data_.GpsFlag_Heading = static_cast<uint32_t>(GpsFlag_Heading);
                asensing_data_.GPS_Age = static_cast<uint32_t>(GPS_Age);
                asensing_data_.Car_Status = static_cast<uint32_t>(Car_Status);
                asensing_data_.Status = static_cast<uint32_t>(Status);
                break;
            }

            //13. CAN ID: 0X507
            case 1287:{
                auto std_Lat = 0.001 * (256 * frame_from_can.data[0] + frame_from_can.data[1]);
                auto std_Lon = 0.001 * (256 * frame_from_can.data[2] + frame_from_can.data[3]);
                auto std_LocalHeight = 0.001 * (256 * frame_from_can.data[4] + frame_from_can.data[5]);
                auto std_Heading = 0.01 * (256 * frame_from_can.data[6] + frame_from_can.data[7]);

                asensing_data_.std_Lat = static_cast<float>(std_Lat);
                asensing_data_.std_Lon = static_cast<float>(std_Lon);
                asensing_data_.std_LocalHeight = static_cast<float>(std_LocalHeight);
                asensing_data_.std_Heading = static_cast<float>(std_Heading);
                break;
            }

            //14. CAN ID:0X508
            case 1288:{
                auto UTC_year = frame_from_can.data[0];
                auto UTC_month = frame_from_can.data[1];
                auto UTC_day = frame_from_can.data[2];
                auto UTC_hour = frame_from_can.data[3];
                auto UTC_min = frame_from_can.data[4];
                auto UTC_sec = frame_from_can.data[5];
                auto UTC_msec = 0.01 * (256 * frame_from_can.data[6] + frame_from_can.data[7]);

                asensing_data_.UTC_year = static_cast<float>(UTC_year);
                asensing_data_.UTC_month = static_cast<float>(UTC_month);
                asensing_data_.UTC_day = static_cast<float>(UTC_day);
                asensing_data_.UTC_hour = static_cast<float>(UTC_hour);
                asensing_data_.UTC_min = static_cast<float>(UTC_min);
                asensing_data_.UTC_sec = static_cast<float>(UTC_sec);
                asensing_data_.UTC_msec = static_cast<double>(UTC_msec);
            break;
            }

            //15. CAN ID:0X35A
            case 858:{
                auto motor_command_torq_rl= 256 * frame_from_can.data[4] + frame_from_can.data[5];
                auto motor_command_torq_rr = 256 * frame_from_can.data[6] + frame_from_can.data[7];
                auto wheel_speed_rl = 0.00868 * (256 * frame_from_can.data[0] + frame_from_can.data[1]);
                auto wheel_speed_rr = 0.00868 * (256 * frame_from_can.data[2] + frame_from_can.data[3]);

                feedback_data_.motor_command_torq_rl = static_cast<float>(motor_command_torq_rl);
                feedback_data_.motor_command_torq_rr = static_cast<float>(motor_command_torq_rr);
                feedback_data_.wheel_speed_rl = static_cast<float>(wheel_speed_rl);
                feedback_data_.wheel_speed_rr = static_cast<float>(wheel_speed_rr);
                feedback_data_.header.stamp = ros::Time::now();

                feedbackDataPublisher_.publish(feedback_data_);
            break;
            }
            




            default:{
                break;
            }
            
        //} //switch end
         //return;
        
        
    }
    end_time= ros::Time::now().toNSec();
    std::cout<<"calc time is: "<<double(end_time - start_time)/10e6<<" ms"<<std::endl;
}
    void CanBus::controlCommandCallback(const fsd_common_msgs::ControlCommand::ConstPtr& controlcommand){
        auto throttle = controlcommand->throttle.data;
        auto steering_angle = 180*(controlcommand->steering_angle.data) / M_PI;
        //调试时使用，直接生成正弦信号
        //auto steering_angle = 10 * sin(0.1 * i) + 0.25;
        //auto throttle = sin(0.1 * i) + 2;
        //++1;
        //为了变为整数，以编码为CAN帧，对和数据进行偏置和放大
        int throttle_ = static_cast<int>(throttle*10);
        int steering_angle_ = static_cast<int>((steering_angle + 50)*1000);

        //1.车速
        throttle_ = std::max(throttle_,0);
        throttle_ = std::min(throttle_,255);
        throttle_low_ = throttle_;
        //2.方向盘转角
        steering_angle_ = std::max(steering_angle_,0);
        steering_angle_ = std::min(steering_angle_,(50 + 50)*1000);
        steering_angle_high_ = steering_angle_ / (256*256);
        steering_angle_mid_ = (steering_angle_ - steering_angle_high_*256*256) / 256;
        steering_angle_low_ = steering_angle_ - steering_angle_high_ * 256*256 - steering_angle_mid_ * 256;
        return;
    }

    void CanBus::asStateCallback(const fsd_common_msgs::AsState::ConstPtr& asstate){
        auto ready = asstate->ready;
        auto sensorstate = asstate->sensorState;
        auto finished = asstate->finished;
        auto end = asstate->end;

        ready_ = static_cast<int>(ready);
        sensorState_ = static_cast<int>(sensorstate);
        finished_ = static_cast<int>(finished);
        end_ = static_cast<int>(end);
        return;
    }

    void CanBus::sendControlCommandAndAsState(void){
        can_msgs::Frame frame;
        frame.header.stamp = ros::Time::now();
        frame.id = 0x305;
        frame.dlc = 8;
        frame.data[0] = steering_angle_low_;
        frame.data[1] = steering_angle_mid_;
        frame.data[2] = steering_angle_high_;
        frame.data[3] = throttle_low_;
        frame.data[4] = ready_;
        frame.data[5] = sensorState_;
        frame.data[6] = finished_;
        frame.data[7] = end_;
        controlCommandPublisher_.publish(frame);
        //std::cout<<"Sucessfully sent can"<<std::endl;
        return;
    }

    void CanBus::run(void){
        sendControlCommandAndAsState();
        asensingDataPublisher_.publish(asensing_data_);
        return;
    }

}
