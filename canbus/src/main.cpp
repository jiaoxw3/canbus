#include <ros/ros.h>
#include "canbus.hpp"

typedef ns_can::CanBus CanBus;

int main(int argc,char** argv){
    ros::init(argc,argv,"canbus");
    ros::NodeHandle nodehandle("~");
    CanBus canbus(nodehandle);
    ros::Rate loop_rate(canbus.node_rate_);

    while(ros::ok()){
        ros::spinOnce();
        canbus.run();
        loop_rate.sleep();
    }
    return 0;
}