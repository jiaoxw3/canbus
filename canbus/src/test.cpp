#include <ros/ros.h>
#include <iostream>
#include "can_msgs/Frame.h"

void callback(const can_msgs::Frame::ConstPtr& frame){
    if(frame->id == 0x122){
        
        int a = frame->data[0];
        /*
        for(int i = 0;i<8;i++){
            std::cout<<std::hex<<frame->data[i]<<std::endl;
        }
        */
        std::cout<<a<<std::endl;
    }
    return;
}


int main(int argc,char** argv){
    ros::init(argc,argv,"cantest");
    ros::NodeHandle nh("~");
    ros::Rate loop(10);
    ros::Publisher pub_frame = nh.advertise<can_msgs::Frame>("/can_send",1);
    ros::Subscriber sub_frame = nh.subscribe<can_msgs::Frame>("/can_receive",1,callback);
    can_msgs::Frame frame;
    frame.header.stamp = ros::Time::now();
    frame.id = 0x122; //echo msg显示的是十进制 id data 都是
    frame.dlc = 8;
    frame.data[0] = 0x10;
    frame.data[2] = 0x20;
    frame.data[7] = 255; //超过255的话会随机显示一个数
    while(ros::ok()){
        ros::spinOnce();
        loop.sleep();
        pub_frame.publish(frame);
    }
    return 0;
}