#ifndef CANBUS_HPP
#define CANBUS_HPP
#include <ros/ros.h>
#include "can_msgs/Frame.h" 
#include "fsd_common_msgs/ControlCommand.h"
#include "fsd_common_msgs/CarState.h"
#include "fsd_common_msgs/CarStateDt.h"
#include "fsd_common_msgs/AsState.h"
#include "fsd_common_msgs/ResAndAmi.h"
#include "fsd_common_msgs/Feedback.h"
#include "fsd_common_msgs/AsensingMessage.h"
#include "nav_msgs/Path.h"
#include "fsd_common_msgs/CanFrames.h"
#include <sys/time.h>

namespace ns_can{
class CanBus{
public:
    //constructor
    CanBus(ros::NodeHandle &nodeHandle);

    //Methods
    void loadParameters();
    void subscribeToTopics();
    void publishToTopics();
    void run();
    void sendControlCommandAndAsState(); // send to ecu
    void calculate_compute_time(struct timeval start_time,struct timeval end_time);
    //data
    int node_rate_ = 30;
    //定圆标定获得 21赛季的coe为7967.96
    double steering_motor_coe_k_ = 6021.6;
    double steering_motor_coe_b_ = -2300.7;
    double init_lat_m_ = 2.63595e+06;
    double init_lon_m_ = -3.51949e+07;

    
    
private:
    ros::NodeHandle nodeHandle_;

    //publisher ---- got can post from scoketcan and send to rossystem 
        //to ros
    ros::Publisher asensingDataPublisher_;
    ros::Publisher resAndAmiPublisher_;
    ros::Publisher steeringMotorDataPublisher_;
    ros::Publisher steeringSensorDataPublisher_;
    ros::Publisher feedbackDataPublisher_;
    ros::Publisher ecuSteeringCommandDataPublisher_;
        //to can
    ros::Publisher controlCommandPublisher_;

    //subscriber ---- get can post from scoketcan
        //from can
    ros::Subscriber rosCanOpenSubscriber_;
        //from ros
    ros::Subscriber controlCommandSubscriber_;
    ros::Subscriber asStateSubscriber_;
    //ros::Subscriber remoteConctrolCommandSubscriber_;

    //callback functions
    //void rosCanOpenCallback(const fsd_common_msgs::CanFrames::ConstPtr& frame); //不用constptr的话会报错，具体原因未知 但可以看看https://answers.ros.org/question/212857/what-is-constptr/
    void rosCanOpenCallback(const can_msgs::Frame::ConstPtr& frame);
    void controlCommandCallback(const fsd_common_msgs::ControlCommand::ConstPtr& controlcommand);
    void asStateCallback(const fsd_common_msgs::AsState::ConstPtr& asstate);


    //self defined msgs
    fsd_common_msgs::AsensingMessage asensing_data_;
    fsd_common_msgs::ResAndAmi res_and_ami_data_;
    fsd_common_msgs::ControlCommand steering_motor_data_;
    fsd_common_msgs::ControlCommand steering_sensor_data_;
    fsd_common_msgs::ControlCommand ecu_steering_command_data_;
    fsd_common_msgs::Feedback feedback_data_; //轮速，力矩
    
    fsd_common_msgs::ControlCommand control_command_;
    fsd_common_msgs::AsState asstate_data_;
    can_msgs::Frame ros_canopen_frame_;
    can_msgs::Frame control_command_frame_;


    //topic names
    //pub
    std::string asensing_data_topic_name_;
    std::string res_and_ami_data_topic_name_;
    std::string steering_motor_data_topic_name_;
    std::string steering_sensor_data_topic_name_;
    std::string feedback_data_topic_name_;
    std::string ecu_steering_command_data_topic_name_;
    std::string control_command_to_socketcan_topic_name_; //sent_messages
    //sub
    std::string ros_canopen_topic_name_; // received_messages
    std::string control_command_topic_name_;
    std::string asstate_data_topic_name_;

    //for visual or test
    struct timeval start_time_;
    struct timeval end_time_;
    
    clock_t mission_start_time;

    int start_index;
    int end_index = 0;
    int choose_mission_index = 0;

    //控制指令相关变量
    int steering_angle_low_ = 254;
    int steering_angle_mid_ = 194;
    int steering_angle_high_ = 0;
    int throttle_low_ = 0;
    
    //状态机/决策相关变量
    int ready_ = 1;
    int sensorState_ = 1;
    int finished_ = 0;
    int end_ = 0;
    int i = 0;
    int i2 = 0;
};
} //namespace_can

#endif //CANBUS_HPP