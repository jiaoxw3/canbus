//样例只是提供一个简单的调用so库的方法供参考，程序接收，与发送函数设置在两个线程中，并且线程没有同步。
//现实中客户编程中，发送与接收函数不能同时调用（不支持多线程），如果在多线程中，一定需要互锁。需要客户自行完善代码。
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <pthread.h>
#include <thread>
#include "controlcan.h"
#include <ctime>
#include <unistd.h>
#include <cstdlib>
#include "unistd.h"
#include "ros/ros.h"
#include <can_msgs/Frame.h>
#include <fsd_common_msgs/ControlCommand.h>
#include <time.h>
#include <fsd_common_msgs/CanFrames.h>
#include <iostream>
#define num_frame_stored 888

int reset = 0;
int start_num = 0;
int end_num = 0;
int l;

VCI_BOARD_INFO pInfo;//用来获取设备信息。
int count=0;//数据列表中，用来存储列表序号。
VCI_BOARD_INFO pInfo1 [50];
int num=0;
//frame going to transmit
can_msgs::Frame frame_from_control;
fsd_common_msgs::CanFrames frame_to_ros;
fsd_common_msgs::CanFrames frame_from_car;
VCI_CAN_OBJ frame2can[1];

void *receive_func(void* param)  //接收线程。
{
	int reclen=0;
	int reclan=0;
	int i,j;
	VCI_CAN_OBJ rec[3000];//接收缓存，设为3000为佳。
	VCI_CAN_OBJ rea[3000];
	int *run=(int*)param;//线程启动，退出控制。
    int ind=0; // channel id
	
	while(1)
	{	
		
		if((reclen=VCI_Receive(VCI_USBCAN2,0,ind,rec,3000,100))>0)//调用接收函数，如果有数据，进行数据处理显示。
		{           	
			
			if(frame_from_car.Frames.size() > num_frame_stored){
				std::cout<<"oversize clear!!!!!"<<std::endl;
                frame_from_car.Frames.clear();
				// start_num = 0;
				// end_num = 0;
            	}

            can_msgs::Frame temp_frame;
			//若没有超过888则还可以继续接收数据所以现在要发的数据开始从之前的末尾开始发
			start_num = frame_from_car.Frames.size();
			//检测时候使用
			std::cout<<"===================="<<std::endl;
			std::cout<<"next start pub number is"<<start_num<<std::endl;
			std::cout<<"once open receive_func receive"<<reclen<<std::endl;
			end_num = start_num + reclen;
			for(int j=0;j<reclen;j++)
			{
				

				for(int k= 0;k<rec[j].DataLen;k++){
					temp_frame.data[k] = rec[j].Data[k];
				}
				temp_frame.dlc = rec[j].DataLen;
				temp_frame.is_extended = false;
				temp_frame.id = rec[j].ID;
				temp_frame.header.stamp = ros::Time::now();
				
				frame_from_car.Frames.push_back(temp_frame);

				//std::cout<<"I get can posts from canB fill it into frame_from_car"<<std::endl;
				std::cout<<"I get can posts from canB "<<frame_from_car.Frames.size()<<std::endl;
				}
		}
		//can channel start
		if((reclan=VCI_Receive(VCI_USBCAN2,0,1,rea,3000,100))>0)//调用接收函数，如果有数据，进行数据处理显示。
		{      
			// if(frame_from_car.Frames.size() > num_frame_stored){
			// 	std::cout<<"oversize clear!!!!!"<<std::endl;
            //     frame_from_car.Frames.clear();
			// 	// start_num = 0;
			// 	// end_num = 0;
            // 	}     	
            can_msgs::Frame temp_frame;
			end_num = start_num + reclan;
			std::cout<<"the pub end number is"<<end_num<<std::endl;
			for(int j=0;j<reclan;j++)
			{
				

				for(int k= 0;k<rea[j].DataLen;k++){
					temp_frame.data[k] = rea[j].Data[k];
				}
				temp_frame.dlc = rea[j].DataLen;
				temp_frame.is_extended = false;
				temp_frame.id = rea[j].ID;
				temp_frame.header.stamp = ros::Time::now();
				
				frame_from_car.Frames.push_back(temp_frame);

				//std::cout<<"I get can posts from canB fill it into frame_from_car"<<std::endl;
				std::cout<<"I get can posts from canA "<<frame_from_car.Frames.size()<<std::endl;
			}
		}
		// usleep(50);	//us
        if((*run)&0x0f){
             std::this_thread::sleep_for(std::chrono::milliseconds(25));
        }
		
	}
}

void subCallBack(const can_msgs::Frame::ConstPtr& frame){
    //ROS_INFO("I heard from control");
    frame_from_control = *frame;
    frame2can[0].ID = frame_from_control.id;
    frame2can[0].SendType = 1;
    frame2can[0].RemoteFlag = 0;
    frame2can[0].ExternFlag = 0;
    frame2can[0].DataLen = 8;
    for(int i = 0;i<8;i++){
        frame2can[0].Data[i] = frame_from_control.data[i];
    }
    return;
}

void canDeviceSetup(){
    //can deive initialize
	num = VCI_FindUsbDevice2(pInfo1); //得到设备个数
	printf(">>USBCAN DEVICE NUM:");printf("%d", num);printf(" PCS");printf("\n");
	while(reset == 0){
		reset = VCI_UsbDeviceReset(VCI_USBCAN2,0,0);
		std::cout<<"we try to reset the canbus"<<std::endl;
	}
	if(VCI_OpenDevice(VCI_USBCAN2,0,0)==1)//打开设备
	{
		printf(">>open deivce success!\n");//打开设备成功
	}else
	{	
		printf(">>open deivce error! , we reset it\n");
		exit(1);
	}
	//初始化参数，严格参数二次开发函数库说明书。
	//VCI——INIT——CONFIG定义了初始化can的配置，调用VCI_InitCAN()时需要用到
	VCI_INIT_CONFIG config;
	//config.AccCode=0x24600000;
	//config.AccMask=0x00600000;
	config.AccCode=0xA1200000;
	// config.AccMask=0x01E00000;
	config.AccMask=0xFFFFFFFF;
	config.Filter=1;//接收所有帧
	config.Timing0=0x00;/*波特率500 Kbps  0x00  0x1C*/
	config.Timing1=0x1C;
	config.Mode=0;//正常模式
	if(VCI_InitCAN(VCI_USBCAN2,0,0,&config)!=1)
	{
		printf(">>Init CAN1 error\n");
		VCI_CloseDevice(VCI_USBCAN2,0);
	}

	if(VCI_StartCAN(VCI_USBCAN2,0,0)!=1)
	{
		printf(">>Start CAN1 error\n");
		VCI_CloseDevice(VCI_USBCAN2,0);
	}
	if(VCI_InitCAN(VCI_USBCAN2,0,1,&config)!=1)
	{
		printf(">>Init CAN2 error\n");
		VCI_CloseDevice(VCI_USBCAN2,0);
	}
	// std::cout<<"we open successed"<<VCI_InitCAN(VCI_USBCAN2,0,1,&config)<<std::endl;

	if(VCI_StartCAN(VCI_USBCAN2,0,1)!=1)
	{
		printf(">>Start CAN2 error\n");
		VCI_CloseDevice(VCI_USBCAN2,0);
	}
    return;
}

int main(int argc,char** argv)
{
    ros::init(argc,argv,"candriver");
    ros::NodeHandle nh;
    ROS_INFO("NOW startup canDriver");

    //subs
    ros::Subscriber controlCommand_asState_Subscriber = nh.subscribe<can_msgs::Frame>("/sent_messages",1,subCallBack);

    //pubs
    // ros::Publisher canFramesPublisher = nh.advertise<fsd_common_msgs::CanFrames>("/received_messages",100);
	ros::Publisher canFramesPublisher = nh.advertise<can_msgs::Frame>("/received_messages",100);

    ros::Rate loop_rate(30);

    //create pthread
    int m_run0=1;
	pthread_t threadid;
	int ret;
	ret=pthread_create(&threadid,NULL,receive_func,&m_run0);

	//int interval = 10; //代表每间隔interval个loop才receive一次

    canDeviceSetup();

	
while(ros::ok()){
        
		// pub
		clock_t start_time;
        clock_t end_time;
        start_time= ros::Time::now().toNSec();
        for(l = start_num; l < end_num ; l++){
			canFramesPublisher.publish(frame_from_car.Frames[l]);
		}
		end_time = ros::Time::now().toNSec();
		// std::cout<<l<<std::endl;
		// std::cout<<"calc time is: "<<double(end_time - start_time)/10e6<<" ms"<<std::endl;
		
		//another pub 
		
		// canFramesPublisher.publish(frame_to_ros);




        //sub
        ros::spinOnce();
        m_run0=0;//暂停线程
        // frame2can[0].Data[0] = 0x11; 
        //VCI_Transmit--发送函数   返回值为实际发送成功的帧数
		if(VCI_Transmit(VCI_USBCAN2,0,1,frame2can,1) == 1)
		{
         std::cout<<"Now send can post to canB"<<std::endl;
			// printf("CAN1 TX ID:0x%08X",frame2can[0].ID);
			// printf("DLC:0x%02X",frame2can[0].DataLen);  //%02X  以2位16进制形式print
			// printf("\n");
		}
		// usleep(50);
        m_run0=1;
		
        loop_rate.sleep();
    }
    return 0;
}
